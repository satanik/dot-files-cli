FROM registry.gitlab.com/satanik/dot-files:latest

RUN apk update \
    && apk upgrade \
    && apk add --no-cache sudo curl git newt busybox-suid

RUN adduser -D -s /bin/bash user
RUN echo 'user ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers.d/user

COPY . /opt/dot-files-cli

USER user

WORKDIR /home/user

RUN bash /opt/dot-files-cli/installer --silent --source-dir /opt/dot-files-cli

RUN dot-files --silent install --auto-updates --source-dir /opt/dot-files
RUN dot-files --silent link --automatic --override

CMD tail -f /dev/null
