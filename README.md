# dot files CLI

## TODO

* [ ] install-suggestions tool for convenience
* [x] add installer script for cli (maybe add flags to run dot-files install)
* [ ] add cli maintenance commands `dot-files cli outdated|update|remove`
* [ ] change dot-files interface `dot-files install|outdated|update|remove|link|unlink`
* [ ] add version check mechanism: tags? VERSION-file? other?
* [x] link files inside candidate-dirs instead of the dirs, to allow customization by the user
* [ ] add tests for docker container
* [x] new updating concept, so that everyones files are updated (installdirs and links)

---





> **NOTICE: This README.md is still work in progress and does not contain enough information to utilise this repository fully**





This is the CLI to install the [dot files](https://gitlab.com/satanik/dot-files), a collection of `dot-files` for the author's convenience. The content of those files was tailored carefully year after year to make it easier to bootstrap a comfortable environment in a new system.

The `dot-files` are composed of a variety of files and directories from different use-cases, i.e. for different tools.

## 1. Components

```console
+ commands/
  + cli-commands/
    - update*
  - cli*
  - install*
  - link*
  - unlink*
  - update*
+ files/
  - dot-files.yaml
- common*
- config.yaml
- Dockerfile
- dot-files*
- dot-files-completion.bash
- installer*
- README.md
- VERSION
```

## 2. Requirements

| Tool | Version |
| ---- | ------- |
| Bash | >= 4.4  |
| tmux | >= 2.6  |
| vim  |         |

## 3. Installation

### 3.1 In Docker for tests

1. [Install Docker](https://docs.docker.com/install/)
2. Run the following commands in the terminal

```bash
$ docker run --rm -it registry.gitlab.com/satanik/dot-files-cli:latest bash --login
# inside container
user at da56b44ba571 in ~
[1]$ ls -lA --color=always | awk '{ print $9 " " $10 " " $11 }'
# shows the locally installed files
.bash_aliases -> /opt/dot-files/candidates/bash/.bash_aliases
.bash_functions -> /opt/dot-files/candidates/bash/.bash_functions
.bash_profile -> /opt/dot-files/candidates/bash/.bash_profile
.bash_prompt -> /opt/dot-files/candidates/bash/.bash_prompt
.bin
.dot-files.conf
.dot-files.yaml
.gitconfig -> /opt/dot-files/candidates/git/.gitconfig
.inputrc -> /opt/dot-files/candidates/input/.inputrc
.profiles
.ssh
.tmux
.tmux.conf -> /opt/dot-files/candidates/tmux/.tmux.conf
.vim
.vimrc -> /opt/dot-files/candidates/vim/.vimrc
```

### 3.2 In the environment

1. Make sure the requirements mentioned [above](#2. Requirements) are satisfied
2. Run the following commands in the terminal

```bash
$ curl -fsSL https://gitlab.com/satanik/dot-files-cli/raw/develop/installer | sudo bash -
$ dot-files --help

dot-files

CLI to manage dot-files
Version: 1.0.0-edge
https://gitlab.com/satanik/dot-files

Usage: dot-files [-s|--silent] [-v|--version] [-h|--help] [command] [args...]

Options:
  -s, --silent    Do not print any messages
  -v, --version   Print the version
  -h, --help      Show this help

Commands:
  install         Install dot-files
  link            Link dot-files to the home directory
  unlink          Remove link of dot-files from the home directory
  update          Update and relink the dot-files to the home directory
  cli             CLI tool related commands
  *               Show this help

$ dot-files install --help
$ dot-files link --help
```

`dot-files` is a CLI that allows downloading the most recent state of the dot-files and also linking, unlinking and updating the the files in ones current user's home directory.

## 4. Usage

> **TBD**
