#!/usr/bin/env bash

# script exits when command fails; use || true to allow failure
set -o errexit
# script exits when trying to access undefined variables
set -o nounset
# enable for debugging
# set -o xtrace
# captures last non-zero exit code and returns this instead of the last one
set -o pipefail

declare SOURCE="${0##*/}"

function usage() {
  cat << EOT

dot-files ${SOURCE}

Command to install dot-files
Version: $(cat ${DOTFILES_WORKDIR}/VERSION)
https://gitlab.com/satanik/dot-files

Usage:
  dot-files ${SOURCE} [-h|--help]
  dot-files [-s|--silent] ${SOURCE} [-n] [--source-dir] [-g|--global] [-u|-m]
  dot-files [-s|--silent] ${SOURCE} [-n] [--source-dir] [-l|--local] [--home|--tmp|--install-dir <path>] [-u|-m]

Inherited options:
  -s, --silent           Do not print any messages

Options:
  -n, --dry-run          Do not make any changes
  -g, --global           Installation is done in /opt/dot-files and needs
                         privilege escalation, since /opt is owned by root
  -l, --local            Installation is done in a user-local location
                         On default the user is prompted to specify the
                         target directory
  --home                 Uses ${HOME}/dot-files as target for installation
  --tmp                  Uses /tmp/dot-files as target for the installation
  --install-dir          Passes path in case of local installation
                         The user must have permissions to write and read
                         inside this directory
  --source-dir           If specified it uses the currently existing installation
  -u, --auto-updates     A cron job is installed in the OS-specific
                         <cronjob-dir>/update-dot-files-[username]
  -m, --manual-updates   Installation of auto-updates is skipped
EOT
  exit 1
}

if [[ "${DOTFILES_WORKDIR:-notset}" = "notset" ]]; then
  printf "Error: This command cannot be called standalone\n" >&2;
  usage
fi

source "${DOTFILES_WORKDIR}/common"

function create_installdir() {
  local -r path="$1"
  if [[ ! -d "$path" ]]; then
    mkdir -p "$path"
    if [[ "$?" -eq 1 ]]; then
      log --warning "You do not have permissions to create path [$path]"
      return 1
    fi
  fi
  INSTALLDIR="$path"
}

function validate_sourcedir() {
  local -r path="$1"
  if [[ ! -d "$path" ]]; then
    log --warning "Error: Source directory [${path}] does not exist"
    return 1
  fi

  if [[ ! -d "${path}/candidates" ]]; then
    log --warning "Error: Source directory [${path}] does not contain any candidates for linking"
    return 1
  fi

  SOURCEDIR="$path"
}

function handle_opts() {
  while test "$#" -gt 0; do
    local opt="$1"
    local -a opts=()

    if [[ "$opt" =~ ^-[^-] ]]; then
      opt="${opt#-}"
      for ((i=0; i < "${#opt}"; i++)); do opts[$i]="-${opt:$i:1}"; done
    else
      opts+=("$opt")
    fi

    for o in "${opts[@]}"; do
      case "$o" in
        -h|--help )
          usage
          exit 0;;
        -n|--dry-run )
          DRY_RUN=true;;
        -g|--global )
          PRIVILEGED=true;;
        -l|--local )
          PRIVILEGED=false;;
        --home )
          if ! create_installdir "${HOME}/dot-files"; then
            log --warning "installation target directory [${HOME}/dot-files] cannot be created"
            usage
            exit 1
          fi;;
        --temp )
          if ! create_installdir "/tmp/dot-files"; then
            log --warning "installation target directory [/tmp/dot-files] cannot be created"
            usage
            exit 1
          fi;;
        --install-dir )
          shift
          if ! create_installdir "$1"; then
            usage
            exit 1
          fi;;
        --source-dir )
          shift
          if ! validate_sourcedir "$1"; then
            usage
            exit 1
          fi;;
        -u|--auto-updates )
          AUTO_UPDATES=true;;
        -m|--manual-updates )
          AUTO_UPDATES=false;;
        *)
          log --warning "unexpected option [$o]"
          usage
          exit 1;;
      esac
    done
    shift
  done
}

function prompt_auto_updates() {
  cat << 'EOT'
   Choice   Description                                                Flag
--------------------------------------------------------------------------------
   y[es]    A cron job is installed in the OS-specific       -u|--auto-updates
            <cronjob-dir>/update-dot-files-[username]
 * N[o]     Installation of auto-updates is skipped          -m|--manual-updates

EOT
  question="Install auto-updates [y/N]: "
  while true; do
    echo -ne "$question"; read yn
    case "${yn:-N}" in
      [Nn]* )
        AUTO_UPDATES=false
        break;;
      [Yy]* )
        AUTO_UPDATES=true
        break;;
    esac
  done
  echo
}

function install_auto_updates() {
  if [[ "${AUTO_UPDATES:-notset}" = "notset" ]]; then
    prompt_auto_updates
  fi

  STATUS="Auto-updates"
  if [[ "${AUTO_UPDATES}" = true ]]; then
    log "Installing cronjob to enable auto-updates"

    local times="0 */4 * * *"
    local cmd="dot-files update"
    local cmd_with_logs
    if [[ "$PRIVILEGED" = true ]]; then
      cmd_with_logs="mkdir -p /var/log/dot-files; ${cmd} >/var/log/dot-files/cron.log 2>/var/log/dot-files/cron.err"
      local path
      if [[ "$OSTYPE" == "darwin"* ]]; then
        path="/usr/lib/cron/tabs/update-dot-files"
      elif [[ "$OSTYPE" == "linux"* ]]; then
        if [[ -d /etc/cron.d ]]; then
          path="/etc/cron.d/update-dot-files"
        elif [[ -d /etc/crontabs ]]; then
          path="/etc/crontabs/update-dot-files"
        fi
      fi
      if [[ "$DRY_RUN" = true ]]; then
        log --no-log "Cronjob would be placed in [$path]"
      elif [[ "${path:+set}" = "set" ]]; then
        echo "${times} $(whoami) ${cmd_with_logs}" > "$path"
        [[ "$OSTYPE" == "darwin"* ]] && chown daemon:wheel "$path" || true
        log "Cronjob was placed in [$path]"
      else
        cat << 'EOT' | log --warning
None of the cron directories exist
    - /usr/lib/cron/tabs/
    - /etc/cron.d/
    - /etc/crontabs/
EOT
      fi
    else
      cmd_with_logs="type logger >/dev/null 2>&1 && (${cmd} |& logger) || (${cmd} >/dev/null 2>&1)"
      (
        crontabs -l 2>/dev/null || true
        printf "${times} ${cmd_with_logs}"
      ) | crontab -
    fi
    out
  else
    log "Skipping installation of auto-updates"
    out
  fi
}

function prompt_privilege() {
  cat << 'EOT'
   Choice   Description                                              Flag
--------------------------------------------------------------------------------
 * Y[es]    Installation is done in a user-local location            -l|--local
   n[o]     Installation is done in /opt/dot-files and needs         -g|--global
            privilege escalation, since /opt is owned by root

EOT
  question="Install in user-local location [Y/n]: "
  while true; do
    echo -ne "$question"; read yn
    case "${yn:-Y}" in
      [Yy]* )
        PRIVILEGED=false
        break;;
      [Nn]* )
        PRIVILEGED=true
        break;;
    esac
  done
  out
}

function prompt_install_location() {
  question="Where should the dot-files be installed [$HOME/dot-files]: "
  while [[ "${INSTALLDIR:-notset}" = "notset" ]]; do
    echo -ne "$question"; read path
    path="$( eval echo ${path:-"${HOME}/dot-files"} )"
    if ! create_installdir "$path"; then
      continue
    fi
  done
}

function install_source() {
  local user

  (
    cd "$INSTALLDIR" && (
      local -i rc=0

      if [[ "$OSTYPE" = "darwin"* ]]; then
        user="$(stat -f '%Su' "$INSTALLDIR")"
      elif [[ "$OSTYPE" = "linux"* ]]; then
        user="$(stat -c '%U' "$INSTALLDIR")"
      else
        user="$(ls -ld ${INSTALLDIR} | awk '{print $3}')"
      fi

      if [[ ! -d ".git" ]]; then
        log "Installing dot-files in [$INSTALLDIR]"
        $(which rm) -rf "*" >/dev/null 2>&1
        git init --quiet
      fi
      git remote show origin >/dev/null 2>&1 || rc="$?"
      if [[ "$rc" -ne 0 ]]; then
        git remote add origin "${DOTFILES_remote_https_repository}"
      fi
      git pull --quiet --depth 1 --strategy-option=theirs origin "${DOTFILES_remote_branch}" >/dev/null 2>&1
      git submodule --quiet init
      git submodule --quiet update
      log "Updating dot-files in [$INSTALLDIR]"
    ) || (
      log --warning "[Error: Installation directory ${INSTALLDIR}] does not exist"
      exit 1
    )
  )
}

function install() {
  STATUS="dot-files install"

  local conf_file="${DOTFILES_WORKDIR}/files/dot-files.yaml"
  local target_file="$HOME/.${conf_file##*/}"
  if [[ "$PRIVILEGED" = true ]]; then
    INSTALLDIR="/opt/dot-files"
    mkdir -p "$INSTALLDIR"
    target_file="/etc/dot-files/config.yaml"
  fi

  prompt_install_location
  if [[ "${SOURCEDIR:-notset}" = "notset" ]]; then
    install_source
  fi
  
  mkdir -p "${target_file%/*}"
  if [[ ! -f "$target_file" ]]; then
    cp "$conf_file" "$target_file"
    log "Created [$target_file]"
  fi
  if [[ "$OSTYPE" == "darwin"* ]]; then
    sed -i "" "s|install_directory:.*|install_directory: ${INSTALLDIR}|" "$target_file"
  elif [[ "$OSTYPE" == "linux"* ]]; then
    sed -i "s|install_directory:.*|install_directory: ${INSTALLDIR}|" "$target_file"
  else
    log --warning "Error: it is unclear how to use sed for this OS"
  fi
  log "Set install_directory in [$target_file] to [${INSTALLDIR}]"
}

function main() {
  declare DRY_RUN=false
  declare PRIVILEGED
  declare AUTO_UPDATES
  declare INSTALLDIR
  declare SOURCEDIR

  handle_opts "$@"

  if [[ "${SOURCEDIR:+set}" = "set" ]]; then
    PRIVILEGED=false
    INSTALLDIR="$SOURCEDIR"
  fi

  if [[ "${PRIVILEGED:-notset}" = "notset" ]]; then
    prompt_privilege
  fi

  if [[ "$PRIVILEGED" = true && "$(id -nu)" != "root" ]]; then
    local rc=0
    exec sudo -n "${DOTFILES_WORKDIR}/dot-files" "${0##*/}" "$@" "--global" || rc="$?"
    if [[ "$rc" -ne 0 ]]; then
      if type whiptail >/dev/null 2>&1; then
        local text="$(cat << 'EOF'
Installing the dot-files in a global location
requireds administrative privilege. Please
authenticate to begin the installation.

[sudo] Password for user $USER:
EOF
)"
        local pass="$(whiptail --title "Authentication required" --passwordbox "$text" 12 50 3>&2 2>&1 1>&3-)"
        exec sudo -S -p '' "${DOTFILES_WORKDIR}/dot-files" "${0##*/}" "$@" <<< "$pass"
      else
        exec sudo "${DOTFILES_WORKDIR}/dot-files" "${0##*/}" "$@"
      fi
    fi
    exit 0
  fi

  install_auto_updates

  install
}

main "$@"