#!/usr/bin/env bash

function _main_install() {
  local cur="${COMP_WORDS[COMP_CWORD]}"
  COMPREPLY=($(compgen -W "-h --help -n --dry-run -g --global -l --local --home --tmp --install-dir --source-dir -u --auto-updates -m --manual-updates" -- "$cur"))
}

function _main_link() {
  local cur="${COMP_WORDS[COMP_CWORD]}"
  COMPREPLY=($(compgen -W "-h --help -n --dry-run -i --interactive -a --automatic -o --override -k --keep" -- "$cur"))
}

function _main_unlink() {
  local cur="${COMP_WORDS[COMP_CWORD]}"
  COMPREPLY=($(compgen -W "-h --help -n --dry-run -i --interactive -a --automatic -r --restore -l --leave" -- "$cur"))
}

function _main_update() {
  local cur="${COMP_WORDS[COMP_CWORD]}"
  COMPREPLY=($(compgen -W "-h --help" -- "$cur"))
}

function _main_cli() {
  local -i i=1
  local cli_index

  while [[ "$i" -lt "$COMP_CWORD" ]]; do
    local s="${COMP_WORDS[i]}"
    case "$s" in
    cli)
      cli_index="$i"
      break;;
    esac
    (( i++ ))
  done

  while [[ "$cli_index" -lt "$COMP_CWORD" ]]; do
    local s="${COMP_WORDS[cli_index]}"
    case "$s" in
      update)
        _main_update
        return;;
    esac
    (( cli_index++ ))
  done

  local -r cur="${COMP_WORDS[COMP_CWORD]}"
  COMPREPLY=($(compgen -W "update" -- "$cur"))
}

function _main() {
  local -i i=1
  local cmd

  while [[ "$i" -lt "$COMP_CWORD" ]]; do
    local s="${COMP_WORDS[i]}"
    case "$s" in
      -*);;
      *)
        cmd="$s"
        break;;
    esac
    (( i++ ))
  done

  if [[ "$i" -eq "$COMP_CWORD" ]]; then
    local -r cur="${COMP_WORDS[COMP_CWORD]}"
    COMPREPLY=($(compgen -W "-s --silent install link unlink update cli help" -- "$cur"))
    return
  fi

  case "$cmd" in
    install) _main_install;;
    link) _main_link;;
    unlink) _main_unlink;;
    update) _main_update;;
    cli) _main_cli;;
    *);;
  esac
}

complete -F _main dot-files